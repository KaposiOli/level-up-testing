import { Kekspress, Keksception } from './kekspress';

describe('Kekspress', () => {
  describe('getPassengers', () => {
    test('should return 0 passengers if the Kekspress is empty', () => {
      const kekspress = new Kekspress(9);

      expect(kekspress.getPassengers()).toHaveLength(0);
    });
    test('should return 1 passenger if the Kekspress has 1 boarded passenger', () => {
      const kekspress = new Kekspress(9);

      kekspress.board('passenger3', 3);

      const passengers = kekspress.getPassengers();

      expect(passengers).toHaveLength(1);
    });
  });

  describe('nextStop', () => {
    test('should remove passengers who get off at the stops', () => {
      const kekspress = new Kekspress(9);

      kekspress.board('passenger3', 3);
      kekspress.board('passenger5', 5);

      kekspress.nextStop(2);

      const passengers = kekspress.getPassengers();

      expect(passengers).toHaveLength(1);
    });
    test('should not remove passenger whose stop is skipped', () => {
      const kekspress = new Kekspress(9);

      kekspress.board('passenger1', 2);
      kekspress.board('passenger3', 2);

      kekspress.nextStop(3);

      const passengers = kekspress.getPassengers();

      expect(passengers).toHaveLength(2);
    });
    test('should use default skipping', () => {
      const kekspress = new Kekspress(9);

      kekspress.board('passenger1', 1);
      kekspress.board('passenger3', 3);

      kekspress.nextStop();

      const passengers = kekspress.getPassengers();

      expect(passengers).toHaveLength(1);
    });
  });

  describe('board', () => {
    test('should add a passenger if he has boarded', () => {
      const kekspress = new Kekspress(9);

      kekspress.board('passenger1', 1);

      const passengers = kekspress.getPassengers();

      expect(passengers).toHaveLength(1);
    });
    test('should throw error if the passenger name is already taken', () => {
      const kekspress = new Kekspress(9);

      kekspress.board('passenger1', 2);
      kekspress.board('passenger2', 2);

      try {
        kekspress.board('passenger2', 2);
      } catch (err) {
        expect(err).toEqual(new Keksception('Name passenger2 already boarded'));
      }
    });
    test('should throw error if the train is full', () => {
      const kekspress = new Kekspress(3);

      kekspress.board('passenger1', 1);
      kekspress.board('passenger2', 2);
      kekspress.board('passenger3', 3);
      kekspress.board('passenger4', 4);

      const passengers = kekspress.getPassengers();

      expect(passengers).toHaveLength(3);
    });
  });

  describe('getOff', () => {
    test('should remove passengers when they get off', () => {
      const kekspress = new Kekspress(9);

      kekspress.board('passenger1', 2);
      kekspress.board('passenger2', 4);

      kekspress.getOff('passenger1');

      const passengers = kekspress.getPassengers();

      expect(passengers).toHaveLength(1);
    });
    test('should not remove passenger if the name is not matching', () => {
      const kekspress = new Kekspress(9);

      kekspress.board('passenger1', 2);

      kekspress.getOff('passenger4');

      const passengers = kekspress.getPassengers();

      expect(passengers).toHaveLength(1);
    });
  });
});
